CREATE SCHEMA `transporte` ;

CREATE TABLE `transporte`.`empresa` (
  `id_empresa` INT NOT NULL AUTO_INCREMENT,
  `tipo_documento` VARCHAR(10) NOT NULL,
  `numero_documento` VARCHAR(45) NOT NULL,
  `nombre_completo` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `departamento` VARCHAR(45) NOT NULL,
  `pais` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NULL,
  PRIMARY KEY (`id_empresa`));


CREATE TABLE `transporte`.`vehiculo` (
  `id_vehiculo` INT NOT NULL AUTO_INCREMENT,
  `placa` VARCHAR(45) NOT NULL,
  `motor` VARCHAR(45) NOT NULL,
  `chasis` VARCHAR(45) NOT NULL,
  `modelo` VARCHAR(45) NOT NULL,
  `fecha_matricula` VARCHAR(45) NOT NULL,
  `pasajeros_sentados` INT NOT NULL,
  `pasajeros_pie` INT NOT NULL,
  `peso_seco` VARCHAR(45) NULL,
  `peso_bruto` VARCHAR(45) NULL,
  `puertas` INT NOT NULL,
  `marca` VARCHAR(45) NOT NULL,
  `linea` VARCHAR(45) NULL,
  `id_empresa` INT NULL,
  PRIMARY KEY (`id_vehiculo`),
  INDEX `id_empresa_idx` (`id_empresa` ASC) VISIBLE,
  CONSTRAINT `id_empresa`
    FOREIGN KEY (`id_empresa`)
    REFERENCES `transporte`.`empresa` (`id_empresa`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

CREATE TABLE `transporte`.`conductor` (
  `id_conductor` INT NOT NULL AUTO_INCREMENT,
  `tipo_identificacion` VARCHAR(10) NOT NULL,
  `numero_identificacion` VARCHAR(45) NOT NULL,
  `nombre_completo` VARCHAR(45) NOT NULL,
  `direccion` VARCHAR(45) NOT NULL,
  `ciudad` VARCHAR(45) NOT NULL,
  `departamento` VARCHAR(45) NOT NULL,
  `pais` VARCHAR(45) NOT NULL,
  `telefono` VARCHAR(45) NULL,
  PRIMARY KEY (`id_conductor`));


CREATE TABLE `transporte`.`conductor_vehiculo` (
  `id_conductor_vehiculo` INT NOT NULL AUTO_INCREMENT,
  `idConductor` INT NOT NULL,
  `idVehiculo` INT NOT NULL,
  PRIMARY KEY (`id_conductor_vehiculo`));

