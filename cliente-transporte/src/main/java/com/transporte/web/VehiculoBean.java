package com.transporte.web;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.transporte.domain.Conductor;
import com.transporte.domain.ConductorVehiculo;
import com.transporte.domain.Empresa;
import com.transporte.domain.Vehiculo;
import org.glassfish.jersey.client.ClientConfig;
import org.primefaces.event.RowEditEvent;

@Named("vehiculoBean")
@RequestScoped
public class VehiculoBean {

    ClientConfig clientConfig = new ClientConfig();
    Client cliente = ClientBuilder.newClient(clientConfig);
    private static Invocation.Builder invocationBuilder;
    private static Response response;
    private static WebTarget webTarget;

    private Vehiculo vehiculoSeleccionado;
    public static int idEmpresaSeleccionada;
    private static int idConductorSeleccionado;

    List<Vehiculo> vehiculos;
    List<ConductorVehiculo> conductoresVehiculos;
    private static List<Integer> idsVehiculosConductor;

    public VehiculoBean() {
    }

    @PostConstruct
    public void inicializar() {
            vehiculos = new ArrayList<>();
            WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/vehiculos");
            vehiculos = webTarget.request(MediaType.APPLICATION_XML).get(Response.class).readEntity(new GenericType<List<Vehiculo>>() {
            });
            vehiculoSeleccionado = new Vehiculo();
    }

    public void editListener(RowEditEvent event) {
        Vehiculo vehiculo = (Vehiculo) event.getObject();
        String path = "/" + vehiculo.getIdVehiculo();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/vehiculos");
        invocationBuilder = webTarget.path(path).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.put(Entity.entity(vehiculo, MediaType.APPLICATION_XML));
    }

    public Vehiculo getVehiculoSeleccionado() {
        return vehiculoSeleccionado;
    }

    public void setVehiculoSeleccionado(Vehiculo vehiculoSeleccionado) {
        this.vehiculoSeleccionado = vehiculoSeleccionado;
    }

    public List<Vehiculo> getVehiculos() {
        return vehiculos;
    }

    public void setVehiculos(List<Vehiculo> vehiculos) {
        this.vehiculos = vehiculos;
    }

    public void agregarVehiculo() {
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/vehiculos");
        invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        response = invocationBuilder.post(Entity.entity(vehiculoSeleccionado, MediaType.APPLICATION_XML));
        this.vehiculos.add(vehiculoSeleccionado);
        this.vehiculoSeleccionado = null;
    }

    public void eliminarVehiculo(Vehiculo vehiculo) {
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/vehiculos");
        String pathEliminar = "/" + vehiculo.getIdVehiculo();
        invocationBuilder = webTarget.path(pathEliminar).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.delete();
        this.vehiculos.remove(vehiculo);
        this.vehiculoSeleccionado = null;
    }

    public void reiniciarVehiculoSeleccionada() {
        this.vehiculoSeleccionado = new Vehiculo();
    }

    public void reiniciarEmpresaSeleccionada(Empresa empresa) {
        idEmpresaSeleccionada = empresa.getIdEmpresa();

        Iterator< Vehiculo> it = vehiculos.iterator();
        while (it.hasNext()) {
            Vehiculo vh = it.next();
            if ((vh.getIdEmpresa() != null && vh.getIdEmpresa() == idEmpresaSeleccionada) || vh.getIdEmpresa() == null) {
                if (vh.getIdEmpresa() != null && vh.getIdEmpresa() == idEmpresaSeleccionada) {
                    vh.setMarcado(true);
                } else {
                    vh.setMarcado(false);
                }
            } else {
                vh.setMarcado(true);
            }
        }
        

    }

    public void reiniciarConductorSeleccionado(Conductor conductor) {
        idConductorSeleccionado = conductor.getIdConductor();

        List<Integer> vehiculosPorIdConductor = new ArrayList<>();
        idsVehiculosConductor = new ArrayList<>();
        conductoresVehiculos = new ArrayList<>();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores_vehiculos");
        conductoresVehiculos = webTarget.request(MediaType.APPLICATION_JSON).get(Response.class).readEntity(new GenericType<List<ConductorVehiculo>>() {
        });
        for (ConductorVehiculo conVeh : conductoresVehiculos) {
            if (conVeh.getIdConductor() == idConductorSeleccionado) {
                vehiculosPorIdConductor.add(conVeh.getIdVehiculo());
                idsVehiculosConductor.add(conVeh.getIdConductorVehiculo());
            }
        }

        Iterator< Vehiculo> it = vehiculos.iterator();
        while (it.hasNext()) {
            Vehiculo vh = it.next();

            if (vehiculosPorIdConductor.contains(vh.getIdVehiculo())) {
                vh.setMarcado(true);
            } else {
                vh.setMarcado(false);
            }

        }

    }

    public void actualizarVehiculosEmpresa() {
        for (Vehiculo v : vehiculos) {
            if (v.isMarcado()) {
                v.setIdEmpresa(idEmpresaSeleccionada);
            } else {
                if (v.getIdEmpresa() != null && v.getIdEmpresa() == idEmpresaSeleccionada) {
                    v.setIdEmpresa(null);
                }
            }
            String path = "/" + v.getIdVehiculo();
            WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/vehiculos");
            invocationBuilder = webTarget.path(path).request(MediaType.APPLICATION_XML);
            response = invocationBuilder.put(Entity.entity(v, MediaType.APPLICATION_XML));
        }
        idEmpresaSeleccionada = 0;

    }

    public void actualizarVehiculosConductores() {
        List<Integer> idVehiculos = new ArrayList<>();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores_vehiculos");
        for (Integer i : idsVehiculosConductor) {
            
            String pathEliminar = "/" + i;
            invocationBuilder = webTarget.path(pathEliminar).request(MediaType.APPLICATION_XML);
            response = invocationBuilder.delete();
        }
        idsVehiculosConductor = null;

        for (Vehiculo vh : vehiculos) {
            if (vh.isMarcado()) {
                invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
                response = invocationBuilder.post(Entity.entity(new ConductorVehiculo(idConductorSeleccionado, vh.getIdVehiculo()), MediaType.APPLICATION_JSON));
            }
        }

        idConductorSeleccionado = 0;

    }

    public static int getIdEmpresaSeleccionada() {
        return idEmpresaSeleccionada;
    }

    public boolean validarEmpresaVehiculo(Integer id){
        return id != null && id != idEmpresaSeleccionada ? true : false;
    }
}
