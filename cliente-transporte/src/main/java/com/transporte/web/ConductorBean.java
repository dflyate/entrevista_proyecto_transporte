
package com.transporte.web;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Named;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.transporte.domain.Conductor;
import org.glassfish.jersey.client.ClientConfig;
import org.primefaces.event.RowEditEvent;

@Named("conductorBean")
@RequestScoped
public class ConductorBean {
    
    ClientConfig clientConfig = new ClientConfig();
    Client cliente = ClientBuilder.newClient(clientConfig);
    private static Invocation.Builder invocationBuilder;
    private static Response response;
    private static WebTarget webTarget;
    
    private Conductor conductorSeleccionado;
    
    List<Conductor> conductores;
    
    public ConductorBean(){
    }
    
    @PostConstruct
    public void inicializar(){
        //inicializar variables
        conductores = new ArrayList<>();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores");
        conductores = webTarget.request(MediaType.APPLICATION_XML).get(Response.class).readEntity(new GenericType<List<Conductor>>(){});
        conductorSeleccionado = new Conductor();
    }
    
    public void editListener(RowEditEvent event){
        Conductor conductor = (Conductor) event.getObject();
        String path= "/" + conductor.getIdConductor();
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores");
        invocationBuilder = webTarget.path(path).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.put(Entity.entity(conductor,MediaType.APPLICATION_XML));
    }

    public Conductor getConductorSeleccionado() {
        return conductorSeleccionado;
    }

    public void setConductorSeleccionado(Conductor conductorSeleccionado) {
        this.conductorSeleccionado = conductorSeleccionado;
    }

    public List<Conductor> getConductors() {
        
        return conductores;
    }

    public void setConductors(List<Conductor> conductores) {
        this.conductores = conductores;
    }
    
    public void agregarConductor(){
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores");
        invocationBuilder = webTarget.request(MediaType.APPLICATION_XML);
        response=invocationBuilder.post(Entity.entity(conductorSeleccionado, MediaType.APPLICATION_XML));
        this.conductores.add(conductorSeleccionado);
        this.conductorSeleccionado = null;
    }
    
    public void eliminarConductor(Conductor conductor){
        WebTarget webTarget = cliente.target("http://localhost:8080/empresa-transporte/webservice").path("/conductores");
        String pathEliminar= "/" + conductor.getIdConductor();
        invocationBuilder = webTarget.path(pathEliminar).request(MediaType.APPLICATION_XML);
        response = invocationBuilder.delete();
        this.conductores.remove(conductor);
        this.conductorSeleccionado = null;
    }
    
    public void reiniciarConductorSeleccionado(){
        this.conductorSeleccionado = new Conductor();
    }
}
